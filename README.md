# 硕士毕业设计指南

硕士是一个介于学士及博士之间的研究生学位（Post-Graduate），拥有硕士学位者通常象征具有基础的独立的思考能力。硕士和博士是人生中非常重要的阶段，是介于学生、社会人的一个过度阶段，在这个阶段不是单纯的学习知识，更多的是通过一个或多个研究项目锻炼自己的思维能力、解决问题能力；同时也不是单纯工作挣钱的，做项目主要的目的是`知行合一`，通过实际的项目锻炼独立自主解决问题的能力。能够达到这个阶段的人虽然逐年增加，但是仍然是整个人群中的少数，因此社会对硕士、博士的要求非常高，但同时收入和前景也会比普通人好很多，这就是所谓的有得有失。

![master](images/master.png)

为了更好的完成学习、研究，请参考 https://gitee.com/pi-lab/research_change_detection ，将自己的制定的研究计划等整理到自己的研究项目，创建上传Gitee项目，方便大家和导师协同。

## 1. 具体的操作流程

1. 大家将自己的研究项目在gitee上创建，[或者fork这个项目到自己的项目](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/HowToForkClone.md)
2. 把我加入到各自的研究项目，具体的说明参考[《Gitee增加用户》](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/gitee_addmember.md)
3. 根据自己的研究目标，制定自己的[《研究计划》](research_planning.md)，主要包括：研究目标，研究思路，关键技术，研究计划，参考资料等。
4. 在调研、阅读相关论文，并进行一些研究、开发工作，撰写开题报告，从而明确自己的研究目标、研究内容。具体写作建议可参考[开题报告注意事项](开题报告注意事项.md),另外可以参考[开题报告示例](doc/开题答辩-sample.pdf)。
5. 研究一段时间之后，将自己的研究工作总结成中期报告。
6. 按照自己指定的研究计划、开题报告，完成每个研究内容，同时积累素材，撰写论文，具体可以参考[《如何写论文》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/paper_writting/README.md)
7. 撰写[《毕业论文》](https://gitee.com/pi-lab/pilab_research_fields/tree/master/tips/learn_writting/thesis)
8. 为了更好的展示自己的研究成果，大家把自己的研究成果素材收集好，编辑一个1-3分钟的视频，通过这个视频让其他人更好的理解自己做的工作。具体可以参考借鉴[《示例成果视频》](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1a5411j7kH) 。
9. 大家准备答辩的PPT并进行答辩，将自己的研究目的、内容、结果、成果通过PPT等展现给评委老师。



## 2. 要求

硕士阶段通过完成一个或者几个科研项目，将所学的知识综合起来去解决一些问题，这个过程锻炼自主学习、分析问题、分解问题、解决问题、总结、撰写论文、编写资料等多方面的能力。大家需要积极主动。

最终硕士论文的要求：

1. 研究背景、意义
2. 相关研究的分析
3. 所提出方法的具体细节
4. 实验
5. 将所提出的方法应用到一个实际的系统，描述系统的目标、系统架构设计、实现、特性、应用



### 2.1 各个阶段的具体要求

1. 硕士一年级，上学期：完成选修课程，完成研究方案的设计，基础知识、基本算法、软件学习；查阅相关文献

2. 硕士一年级，下学期：撰写研究综述；完成研究的初步实现，开始撰写论文

3. 硕士二年级，上学期：继续完善研究，补充实验，投稿

4. 硕士二年级，下学期：将所研究的方法应用到一个系统

5. 硕士三年级，上学期：准备毕业论文，将硕士阶段所做的工作进行总结




### 2.2 各个阶段需要完成的材料

* 开题报告： 明确自己的研究目标、研究内容。具体写作建议可参考[开题报告注意事项](开题报告注意事项.md)， 另外可以参考[开题报告示例](doc/开题答辩-sample.pdf)。 
  * `\\192.168.1.4\data\pi-lab\硕士-博士-毕业参考资料\硕士\开题`
* 中期考核：  
  * `\\192.168.1.4\data\pi-lab\硕士-博士-毕业参考资料\硕士\中期考核`
* 毕业论文：  
  * `\\192.168.1.4\data\pi-lab\汇总-毕业资料\硕士`  
  * `\\192.168.1.4\data\pi-lab\硕士-博士-毕业参考资料\硕士\毕业论文`

* 完成所有的工作之后，将所有的硕士阶段的材料汇总到
  * `\\192.168.1.4\data\pi-lab\汇总-毕业资料\硕士`

## 2. 基础知识学习

大家评估自己的能力，并制定有针对性的基础能力学习：

* 如果编程能力比较弱，可以好好学习编程： [《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
* 如果做机器学习方面的研究，可以自学[《机器学习》](https://gitee.com/pi-lab/machinelearning_notebook)，在线教程视频：[《B站 - 机器学习》](https://www.bilibili.com/video/BV1oZ4y1N7ei/)
* 如果做视觉方面的研究，可以自学[《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)



## 3. 参考资料

### 3.1 毕业设计参考
* https://gitee.com/pi-lab/research_change_detection
* 建议大家使用[《Latex》](https://gitee.com/pi-lab/SummerCamp/tree/master/tool/latex)来撰写自己的毕业论文，[《西北工业大学 - 硕士论文LaTex模版与示例》](https://gitee.com/pi-lab/template_master)
* [《毕业论文指南》](https://gitee.com/pi-lab/pilab_research_fields/tree/master/tips/learn_writting/thesis)



### 3.2 工具的使用教程等

由于需要使用Markdown和Git，因此大家可以静下心好好学习一下这两个工具
* [Markdown](https://gitee.com/pi-lab/learn_programming/6_tools/markdown)
* [Git](https://gitee.com/pi-lab/learn_programming/6_tools/git)

其他的参考资料：
* [Code Cook - 编程参考代码，技巧集合](https://gitee.com/pi-lab/code_cook)
* [Linux](https://gitee.com/pi-lab/learn_programming/6_tools/linux)
* [CMake](https://gitee.com/pi-lab/learn_programming/6_tools/cmake)



### 3.3 编程、技能

在学习、研究过程，如果觉得自己的基础知识不够，需要补充学习，可以参考如下的教程：

1.  [《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
2. 飞行器智能感知与控制实验室-培训教程与作业
    - [《飞行器智能感知与控制实验室-暑期培训教程》](https://gitee.com/pi-lab/SummerCamp)
    - [《飞行器智能感知与控制实验室-暑期培训作业》](https://gitee.com/pi-lab/SummerCampHomework)
3. 机器学习教程与作业
    - [《机器学习教程》](https://gitee.com/pi-lab/machinelearning_notebook)
    - [《机器学习课程作业》](https://gitee.com/pi-lab/machinelearning_homework)
4. [《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)
5. [飞行器智能感知与控制实验室-研究课题](https://gitee.com/pi-lab/pilab_research_fields)
6. [编程代码参考、技巧集合](https://gitee.com/pi-lab/code_cook)
    - 可以在这个代码、技巧集合中找到某项功能的示例，从而加快自己代码的编写
